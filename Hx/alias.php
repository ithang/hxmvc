<?php

/**
 *  一些常用操作的别名处理
 *  需要bootstrap的时候配置好
 */

use Hx\App;
use Hx\Event;
use Hx\View;

/**
 * @param string $key
 * @return null|string
 */
function getApp($key) {
    return App::getApp($key);
}

/**
 * @param string $name
 * @return array|string
 */
function getConfig($name) {
    return App::getConfig($name);
}

/**
 * @param string $className
 * @return bool
 */
function import($className) {
    return App::import($className, true);
}

/**
 * @param string $exec
 * @param array $args
 * @return mixed
 */
function doAction($exec, $args = array()) {
    return App::Action($exec, $args);
}

/**
 * @param string $exec
 * @param array $args
 * @return mixed
 */
function doModel($exec, $args = array()) {
    return App::Model($exec, $args);
}

/**
 * @param string $message
 * @param string $class
 * @param int $status
 */
function doError($message, $class, $status = 500) {
    App::error($message, $class, $status);
}

/**
 * @param string $name
 * @param array $args
 * @return mixed|null
 */
function doTrigger($name, $args = array()) {
    return Event::trigger($name, $args);
}

/**
 * @param string $file
 * @param int $compile
 * @param array $data
 * @return string
 */
function doView($file, $compile = 0, $data = array()) {
    return View::display($file, $compile, $data);
}

/**
 * @param string $name
 * @return \Hx\Db
 */
function getDb($name = 'default') {
    return App::Db($name);
}

/**
 * @param string $table
 * @param string $cols
 * @return \Hx\Sql
 */
function getSql($table, $cols = '*') {
    return App::Sql($table, $cols);
}
